FROM node:17-alpine

WORKDIR /usr/src/app

COPY package*.json ./
RUN npm install
COPY index.html .
COPY server.js .

#For docker-compose
#ENV PRODUCTS_SERVICE="products_app_1"
#ENV SHOPPING_CART_SERVICE="shopping_cart_app_1"

#For kubernetes
ENV PRODUCTS_SERVICE="products"
ENV SHOPPING_CART_SERVICE="shopping_cart"

EXPOSE 3000
CMD ["npm", "start"]
